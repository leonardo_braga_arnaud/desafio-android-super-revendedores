# Conclusão do Projeto #

Todos itens obrigatórios e adicionais foram feitos, a saber:

* Lista de repositórios
* Paginação
* Lista de Pulls
* Abertura da url no browser ao clicar no pull
* Gestão de depencencias com Gradle
* Mapeamento Gson
* Framework de comunicação com a API
* Alguns testes funcionais e unitarios
* Layout para telas pequenas e telas de telas 7 polegadas
* Cache de imagens
* Cache de Dados com acesso off-line.

## Libs usadas ##

* support - Suporte a versão mais antigas
* dagger - Injeção de dependencias
* retrofit - Comunicação com a API
* lifeCycle - Gerenciamento do ciclo de vida para o React do Android
* room - Auxilia com o gerenciamento  do banco de dados SQLite
* glide - Gerenciador de imagens
* constraintLayoutVersion - Layout moderno
* mugen - Auxilia na paginação das listagens
* joda - Foi usado para trabalhar com datas e tratar fuso horário
* junit, testRunner, testEspresso, mockito - Testes

## Observações ##

* Tive que limitar o mínimo para versão 21 (Android 5.0) por que a API do Github causa problemas de certificado em Android antigos. Como não era escopo do teste resolver essa questão, achei mais prático limitar.
* Usei a arquitetura MVVM que é a recomendada atualmente pela Google.


Obrigado pela oportunidade de participar do processo seletivo.
Abs,

Leonardo Braga Arnaud
leonardo.arnaud.java@gmail.com
(21) 97266-8312


# Criar um aplicativo de consulta a API do [GitHub](https://github.com)#

Criar um aplicativo para consultar a [API do GitHub](https://developer.github.com/v3/) e trazer os repositÃ³rios mais populares de Java. Basear-se no mockup fornecido:

![Lista de repositorios](https://bitbucket.org/adminsuper/desafio-android-super-revendedores/raw/95459e655c07e52ca3906624ca831e12913b664c/image/list.png)
![Lista de PR](https://bitbucket.org/adminsuper/desafio-android-super-revendedores/raw/95459e655c07e52ca3906624ca831e12913b664c/image/repo.png =250x250)

### **Deve conter** ###

- __Lista de repositÃ³rios__. Exemplo de chamada na API: `https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1`
  * PaginaÃ§Ã£o na tela de lista, com endless scroll / scroll infinito (incrementando o parÃ¢metro `page`).
  * Cada repositÃ³rio deve exibir Nome do repositÃ³rio, DescriÃ§Ã£o do RepositÃ³rio, Nome / Foto do autor, NÃºmero de Stars, NÃºmero de Forks
  * Ao tocar em um item, deve levar a lista de Pull Requests do repositÃ³rio
- __Pull Requests de um repositÃ³rio__. Exemplo de chamada na API: `https://api.github.com/repos/<criador>/<repositÃ³rio>/pulls`
  * Cada item da lista deve exibir Nome / Foto do autor do PR, TÃ­tulo do PR, Data do PR e Body do PR
  * Ao tocar em um item, deve abrir no browser a pÃ¡gina do Pull Request em questÃ£o


### **OBS** ###

A foto do mockup Ã© meramente ilustrativa.

### Adicionais ###

* GestÃ£o de dependencias no projeto. Ex: [Gradle]
* Mapeamento json -> Objeto
* Framework para ComunicaÃ§Ã£o com API
* Testes unitÃ¡rios no projeto
* Testes funcionais
* App Universal
* Cache de Imagens
* Cache dos dados


### **SugestÃµes** ###

Pode-se utilizar das libs que preferir. HÃ¡ muitos lugares de referÃªncia como [Android Arsenal](https://android-arsenal.com/)

Caso tenha alguma dÃºvida, o [CodePath](https://guides.codepath.com/android) Ã© um bom lugar para dar uma olhada. NÃ£o impedimos
que outros lugares sejam consultados.

Use de preferÃªncia **JAVA** para o desafio, mas saber Kotlin Ã© um diferencial.

Bons usos do material design sÃ£o apreciados pelo nosso designer! :D

Coloque o link do github das libs que usar no Read.me ou na Doc do PR que fizer, para que possamos analisar.

### **Processo de submissÃ£o** ###
O candidato deverÃ¡ implementar a soluÃ§Ã£o e enviar um PR (pull request) para este repositÃ³rio com a soluÃ§Ã£o.

Boa Sorte!
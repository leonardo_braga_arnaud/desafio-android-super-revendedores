package br.eti.arnaud.desafioandroidsuperrevendedores

import android.arch.lifecycle.Observer
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.AppDatabase
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.dao.RepoDao
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Repo
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.io.IOException

@Suppress("DEPRECATION")
@RunWith(AndroidJUnit4::class)
class DatabaseTest {

    private var repoDao: RepoDao? = null
    private var db: AppDatabase? = null

    private val repoMock = TestUtils.createMockRepo()

    @Mock
    lateinit var observer: Observer<List<Repo>>

    @Before
    fun createDb() {
        MockitoAnnotations.initMocks(this)
        val context = InstrumentationRegistry.getTargetContext()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        repoDao = db?.repoDao()
    }

    @Test
    @Throws(Exception::class)
    fun writeRepoAndReadInList() {
        val list = listOf(repoMock)
        repoDao?.insertAll(list)
        val all = repoDao?.getAllRepositoriesWithLanguage("Java")
        all?.observeForever(observer)
        verify(observer).onChanged(list)
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db?.close()
    }

}

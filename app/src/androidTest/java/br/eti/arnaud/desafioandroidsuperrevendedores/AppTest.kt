package br.eti.arnaud.desafioandroidsuperrevendedores

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.test.runner.AndroidJUnit4
import android.test.UiThreadTest
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Pull
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Repo
import br.eti.arnaud.desafioandroidsuperrevendedores.vm.PullsViewModel
import br.eti.arnaud.desafioandroidsuperrevendedores.vm.ReposViewModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@Suppress("DEPRECATION")
@RunWith(AndroidJUnit4::class)
class AppTest {

    private var reposViewModel = ReposViewModel()
    private var pullsViewModel = PullsViewModel()

    private val repoMock = TestUtils.createMockRepo()


    private val pullMock = TestUtils.createMockPull()

    @Mock
    lateinit var repoObserver: Observer<List<Repo>>

    @Mock
    lateinit var pullObserver: Observer<List<Pull>>

    @Before
    fun setupTravelListViewModel() {
        MockitoAnnotations.initMocks(this)
        reposViewModel.start()
        pullsViewModel.start()
    }

    @Test
    @UiThreadTest
    fun reposPageShouldResetWhenRefreshing() {

        reposViewModel.refresh()

        assertEquals(1, reposViewModel.getCurrentPage())
    }

    @Test
    @UiThreadTest
    fun shouldGetRepoAfterUpdate() {

        reposViewModel.reposObservable.observeForever(repoObserver)

        (reposViewModel.reposObservable as MutableLiveData).value = listOf(repoMock)

        verify(repoObserver).onChanged(listOf(repoMock))
    }

    @Test
    @UiThreadTest
    fun shouldGetPullAfterUpdate() {

        val observable = pullsViewModel.pullsObservable("","")
        observable.observeForever(pullObserver)

        (observable as MutableLiveData).value = listOf(pullMock)

        verify(pullObserver).onChanged(listOf(pullMock))
    }

}

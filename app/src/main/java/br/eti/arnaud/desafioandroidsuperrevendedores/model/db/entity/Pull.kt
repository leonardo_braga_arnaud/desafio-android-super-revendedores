package br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data.Base
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data.User
import java.util.*

@Entity
data class Pull(

        @PrimaryKey
        @ColumnInfo(name = "pull_id")
        val id: Long,

        @ColumnInfo(name = "pull_url")
        val html_url: String,

        @Embedded
        val base: Base,

        @Embedded
        val user: User,

        @ColumnInfo(name = "pull_title")
        val title: String,

        @ColumnInfo(name = "pull_body")
        val body: String,

        @ColumnInfo(name = "pull_updated_at")
        val updated_at: Date)
package br.eti.arnaud.desafioandroidsuperrevendedores.model.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Pull

@Dao
interface PullDao {

    @Query("SELECT * FROM pull WHERE repo_name = :repoName ORDER BY pull_updated_at DESC")
    fun getAllPulls(repoName: String): LiveData<List<Pull>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(repos: List<Pull>)

}
package br.eti.arnaud.desafioandroidsuperrevendedores.di

import android.app.Application
import android.arch.persistence.room.Room
import android.content.SharedPreferences
import android.preference.PreferenceManager
import br.eti.arnaud.desafioandroidsuperrevendedores.R
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.AppDatabase
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.GsonUTCDateAdapter
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
                .connectTimeout(application.getString(R.string.connect_timeout).toLong(), TimeUnit.SECONDS)
                .writeTimeout(application.getString(R.string.write_timeout).toLong(), TimeUnit.SECONDS)
                .readTimeout(application.getString(R.string.timeout).toLong(), TimeUnit.SECONDS)
                .cache(Cache(application.cacheDir, application.getString(R.string.max_cache_size).toLong()))
                .addInterceptor(logging)
                .build()

        val dateFormat = application.getString(R.string.default_date_format)
        val utcDateAdapterGson = GsonBuilder()
                .registerTypeAdapter(Date::class.java, GsonUTCDateAdapter(dateFormat))
                .create()

        return Retrofit.Builder()
                .baseUrl(application.getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(utcDateAdapterGson))
                .client(client)
                .build()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, application.getString(R.string.db_name))
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application.applicationContext)
    }

}
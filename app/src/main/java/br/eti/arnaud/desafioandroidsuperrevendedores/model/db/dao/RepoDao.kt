package br.eti.arnaud.desafioandroidsuperrevendedores.model.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Repo

@Dao
interface RepoDao {

    @Query("SELECT * FROM Repo WHERE repo_language = :language ORDER BY repo_stargazers_count DESC")
    fun getAllRepositoriesWithLanguage(language: String): LiveData<List<Repo>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(repos: List<Repo>)

    @Query("DELETE FROM Repo WHERE repo_language = :language")
    fun deleteAllWithLanguage(language: String)

}
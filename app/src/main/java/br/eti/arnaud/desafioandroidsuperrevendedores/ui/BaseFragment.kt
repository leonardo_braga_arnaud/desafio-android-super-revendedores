package br.eti.arnaud.desafioandroidsuperrevendedores.ui

import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.View

abstract class BaseFragment : Fragment() {

    private lateinit var listener: OnBaseFragmentInteractionListener

    fun showSnackbar(messageResId: Int?) {
        if (view == null || messageResId == null) return
        Snackbar.make(view!!, messageResId, Snackbar.LENGTH_LONG).show()
    }

    fun loading(b: Boolean?) {
        listener.getProgressBar().visibility = if (b == true) View.VISIBLE else View.GONE
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnBaseFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }

    }

    interface OnBaseFragmentInteractionListener{
        fun getProgressBar(): View
    }

}
package br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.api

import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Pull
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data.GithubRepositories
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {

    @GET("/search/repositories")
    fun getRepositories(
            @Query("q") query: String,
            @Query("sort") sort: String,
            @Query("page") page: Int
    ): Call<GithubRepositories>

    @GET("/repos/{userName}/{repoName}/pulls")
    fun getPulls(
            @Path("userName") userName: String,
            @Path("repoName") repoName: String
    ): Call<List<Pull>>
}
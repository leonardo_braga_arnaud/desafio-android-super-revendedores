package br.eti.arnaud.desafioandroidsuperrevendedores.model.remote

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer

import org.joda.time.DateTime
import org.joda.time.DateTimeZone

import java.lang.reflect.Type
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.TimeZone

class GsonUTCDateAdapter(dateFormatString: String) : JsonSerializer<Date>, JsonDeserializer<Date> {

    private val dateFormat: DateFormat

    init {
        dateFormat = SimpleDateFormat(dateFormatString, Locale.US)
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
    }

    @Synchronized
    override fun serialize(date: Date, type: Type, jsonSerializationContext: JsonSerializationContext): JsonElement {
        return JsonPrimitive(dateFormat.format(date))
    }

    @Synchronized
    override fun deserialize(jsonElement: JsonElement, type: Type,
                             jdc: JsonDeserializationContext): Date {
        try {
            val date = dateFormat.parse(jsonElement.asString)
            val localDate = DateTime(date,
                    DateTimeZone.forTimeZone(TimeZone.getDefault()))
            return localDate.toDate()
        } catch (e: ParseException) {
            throw JsonParseException(e)
        }

    }
}

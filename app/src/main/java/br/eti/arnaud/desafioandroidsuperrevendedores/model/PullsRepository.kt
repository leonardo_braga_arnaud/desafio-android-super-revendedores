package br.eti.arnaud.desafioandroidsuperrevendedores.model

import android.arch.lifecycle.LiveData
import android.content.SharedPreferences
import android.os.AsyncTask
import br.eti.arnaud.desafioandroidsuperrevendedores.App
import br.eti.arnaud.desafioandroidsuperrevendedores.R
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.AppDatabase
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Pull
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.api.GithubService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class PullsRepository: BaseRepository() {

    lateinit var retrofit: Retrofit
        @Inject set

    lateinit var db: AppDatabase
        @Inject set

    lateinit var sharedPreferences: SharedPreferences
        @Inject set

    var pullsCall: Call<List<Pull>>? = null

    private var totalPages: Int

    init {
        App.instance.appComponent.inject(this)
        totalPages = sharedPreferences.getInt(TOTAL_PAGES, 1)
    }

    fun pullsObservable(repoName: String): LiveData<List<Pull>> {
        return db.pullDao().getAllPulls(repoName)
    }

    fun loadData(userName: String, repoName: String) {
        setLoading(true)
        pullsCall?.cancel()
        pullsCall = retrofit.create(GithubService::class.java).getPulls(userName, repoName)
        pullsCall?.enqueue(object : Callback<List<Pull>> {
            override fun onResponse(call: Call<List<Pull>>?, response: Response<List<Pull>>?) {
                setLoading(false)
                if (response?.isSuccessful == true) {
                    val pulls = response.body()
                    if (pulls != null) {
                        AsyncTask.execute {
                            db.pullDao().insertAll(pulls)
                        }
                        return
                    }
                }
                postMessage(R.string.sync_error)

            }

            override fun onFailure(call: Call<List<Pull>>?, t: Throwable?) {
                setLoading(false)
                postMessage(R.string.sync_error)
            }
        })
    }

    companion object {
        const val TOTAL_PAGES = "total_pages"
    }
}
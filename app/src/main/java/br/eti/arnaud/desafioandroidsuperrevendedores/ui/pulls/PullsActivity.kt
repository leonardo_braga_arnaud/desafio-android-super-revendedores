package br.eti.arnaud.desafioandroidsuperrevendedores.ui.pulls

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import br.eti.arnaud.desafioandroidsuperrevendedores.R
import br.eti.arnaud.desafioandroidsuperrevendedores.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_pulls.*

class PullsActivity : BaseActivity() {

    private lateinit var userName: String
    private lateinit var repoName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userName = restoreString(USER_NAME, savedInstanceState)
        repoName = restoreString(REPO_NAME, savedInstanceState)

        setContentView(R.layout.activity_pulls)

        setupToolbar()

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, PullsFragment.newInstance(userName, repoName))
                .commit()


    }

    private fun setupToolbar() {
        pullsToolbar.title = ""
        setSupportActionBar(pullsToolbar)
        supportActionBar?.title = "Pulls: $repoName"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        saveString(USER_NAME, userName, outState)
        saveString(REPO_NAME, repoName, outState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            } else -> super.onOptionsItemSelected(item)
        }
    }

    override fun getProgressBar(): View {
        return pullsProgressBar
    }

    companion object {
        const val USER_NAME = "user_name"
        const val REPO_NAME = "repo_name"

        fun start(context: Context, userName: String, repoName: String){
            val bundle = Bundle()
            bundle.putString(USER_NAME, userName)
            bundle.putString(REPO_NAME, repoName)
            val intent = Intent(context, PullsActivity::class.java)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }

}

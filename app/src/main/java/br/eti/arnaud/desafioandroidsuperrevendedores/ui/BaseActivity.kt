package br.eti.arnaud.desafioandroidsuperrevendedores.ui

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.eti.arnaud.desafioandroidsuperrevendedores.R

abstract class BaseActivity : AppCompatActivity(), BaseFragment.OnBaseFragmentInteractionListener {

    fun saveString(key: String, value: String, bundle: Bundle?) {
        bundle?.putString(key, value)
        getSharedPreferences(getString(R.string.app_preferences_key), Context.MODE_PRIVATE)
                .edit()
                .putString(key, value)
                .apply()
    }

    fun restoreString(key: String, bundle: Bundle?): String {
        return bundle?.getString(key)
                ?: intent?.extras?.getString(key)
                ?: getSharedPreferences(getString(R.string.app_preferences_key), Context.MODE_PRIVATE)
                        .getString(key, "")
    }

}
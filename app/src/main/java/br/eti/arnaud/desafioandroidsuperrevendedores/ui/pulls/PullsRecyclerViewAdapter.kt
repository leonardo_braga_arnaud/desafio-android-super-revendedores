package br.eti.arnaud.desafioandroidsuperrevendedores.ui.pulls

import android.support.v7.widget.RecyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.eti.arnaud.desafioandroidsuperrevendedores.R
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Pull

import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_pull.view.*
import java.text.DateFormat

class PullsRecyclerViewAdapter(private var _values: List<Pull>,
                               private val listener: Listener) :
        RecyclerView.Adapter<PullsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_pull, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item = _values[position]
        val iv = holder.itemView

        holder.item?.let {
            iv.titleTextView.text = it.title
            iv.descriptionTextView.text = it.body
            iv.userNameTextView.text = it.user.login
            iv.dateTextView.text = DateFormat
                    .getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT)
                    .format(it.updated_at)

            Glide.with(iv.context)
                    .load(it.user.avatar_url)
                    .apply(RequestOptions.circleCropTransform())
                    .into(iv.pullImageView)

            iv.setOnClickListener { _ ->
                listener.onPullItemClick(it.html_url)
            }
        }
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView.context).clear(holder.itemView.pullImageView)
    }

    override fun getItemCount(): Int {
        return _values.size
    }

    fun updateValues(list: List<Pull>) {
        val lastPos = _values.size - 1
        _values = list
        if (list.size > _values.size) {
            notifyItemInserted(lastPos)
        } else {
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var item: Pull? = null
    }

    interface Listener {
        fun onPullItemClick(url: String)
    }
}

package br.eti.arnaud.desafioandroidsuperrevendedores.di

import br.eti.arnaud.desafioandroidsuperrevendedores.model.PullsRepository
import br.eti.arnaud.desafioandroidsuperrevendedores.model.ReposRepository
import br.eti.arnaud.desafioandroidsuperrevendedores.vm.PullsViewModel
import br.eti.arnaud.desafioandroidsuperrevendedores.vm.ReposViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(reposViewModel: ReposViewModel)
    fun inject(reposRepository: ReposRepository)
    fun inject(pullsViewModel: PullsViewModel)
    fun inject(pullsRepository: PullsRepository)

}
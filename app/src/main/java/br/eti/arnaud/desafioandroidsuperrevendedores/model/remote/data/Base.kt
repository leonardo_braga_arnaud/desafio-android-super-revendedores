package br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data

import android.arch.persistence.room.Embedded
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Repo

data class Base(

        @Embedded
        val repo: Repo

)
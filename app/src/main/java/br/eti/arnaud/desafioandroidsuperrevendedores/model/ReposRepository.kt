package br.eti.arnaud.desafioandroidsuperrevendedores.model

import android.arch.lifecycle.LiveData
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import br.eti.arnaud.desafioandroidsuperrevendedores.App
import br.eti.arnaud.desafioandroidsuperrevendedores.R
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.AppDatabase
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Repo
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.api.GithubService
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data.GithubRepositories
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class ReposRepository: BaseRepository() {

    lateinit var retrofit: Retrofit
        @Inject set

    lateinit var db: AppDatabase
        @Inject set

    lateinit var sharedPreferences: SharedPreferences
        @Inject set

    var githubRepositoriesCall: Call<GithubRepositories>? = null

    var totalPages: Int

    init {
        App.instance.appComponent.inject(this)
        totalPages = sharedPreferences.getInt(TOTAL_PAGES, 1)
    }

    fun reposObservable(language: String): LiveData<List<Repo>> {
        return db.repoDao().getAllRepositoriesWithLanguage(language)
    }

    fun loadPage(page: Int, language: String) {
        setLoading(true)
        val encodedLang = Uri.encode(language.toLowerCase().capitalize())
        val query = "language:$encodedLang"
        val sort = "stars"
        githubRepositoriesCall?.cancel()
        githubRepositoriesCall = retrofit.create(GithubService::class.java).getRepositories(query, sort, page)
        githubRepositoriesCall?.enqueue(object : Callback<GithubRepositories> {
            override fun onResponse(call: Call<GithubRepositories>?, response: Response<GithubRepositories>?) {
                setLoading(false)
                if (response?.isSuccessful == true) {
                    val githubRepositories = response.body()
                    if (githubRepositories != null) {
                        AsyncTask.execute {
                            totalPages = (githubRepositories.total_count / 30).toInt()
                            sharedPreferences.edit().putInt(TOTAL_PAGES, totalPages).apply()
                            val isRefreshing = page == 1 && totalPages != 0
                            if (isRefreshing) {
                                db.repoDao().deleteAllWithLanguage(language)
                            }
                            db.repoDao().insertAll(githubRepositories.items)
                        }
                        return
                    }
                }
                postMessage(R.string.sync_error)

            }

            override fun onFailure(call: Call<GithubRepositories>?, t: Throwable?) {
                setLoading(false)
                postMessage(R.string.sync_error)
            }
        })
    }

    companion object {
        const val TOTAL_PAGES = "total_pages"
    }
}
package br.eti.arnaud.desafioandroidsuperrevendedores.ui.repos

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.eti.arnaud.desafioandroidsuperrevendedores.R

import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Repo
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_repo.view.*

class ReposRecyclerViewAdapter(private var _values: List<Repo>,
                               private val listener: Listener) :
        RecyclerView.Adapter<ReposRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_repo, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item = _values[position]
        val iv = holder.itemView

        holder.item?.let {
            iv.titleTextView.text = it.name
            iv.descriptionTextView.text = it.description
            iv.starsTextView.text = it.stargazers_count.toString()
            iv.pullsTextView.text = it.open_issues_count.toString()
            iv.userNameTextView.text = it.owner.login

            Glide.with(iv.context)
                    .load(it.owner.avatar_url)
                    .apply(RequestOptions.circleCropTransform())
                    .into(iv.repoImageView)

            iv.setOnClickListener { _ ->
                listener.onRepoItemClick(it.owner.login, it.name)
            }
        }
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView.context).clear(holder.itemView.repoImageView)
    }

    override fun getItemCount(): Int {
        return _values.size
    }

    fun updateValues(list: List<Repo>) {
        val lastPos = _values.size - 1
        _values = list
        if (list.size > _values.size) {
            notifyItemInserted(lastPos)
        } else {
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var item: Repo? = null
    }

    interface Listener {
        fun onRepoItemClick(userName: String, repoName: String)
    }
}

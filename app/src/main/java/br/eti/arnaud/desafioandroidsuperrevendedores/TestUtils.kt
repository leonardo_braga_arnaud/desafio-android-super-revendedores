package br.eti.arnaud.desafioandroidsuperrevendedores

import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Pull
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Repo
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data.Base
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data.Owner
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data.User
import java.util.*

class TestUtils {

    companion object {

        fun createMockRepo(): Repo {
            return Repo(1,"", createMockOwner(), "", 0, 0, "Java", "")
        }

        private fun createMockOwner(): Owner {
            return Owner(1, "", "", "")
        }

        private fun createMockUser(): User {
            return User(1, "", "", "")
        }

        fun createMockPull(): Pull {
            return Pull(1, "", Base(createMockRepo()), createMockUser(), "", "", Date())
        }

    }


}
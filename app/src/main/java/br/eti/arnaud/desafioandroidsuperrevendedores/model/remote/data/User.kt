package br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data

import android.arch.persistence.room.ColumnInfo

data class User(

        @ColumnInfo(name = "user_id")
        val id: Long,

        @ColumnInfo(name = "user_url")
        val url: String,

        @ColumnInfo(name = "user_login")
        val login: String,

        @ColumnInfo(name = "user_avatar_url")
        val avatar_url: String

)
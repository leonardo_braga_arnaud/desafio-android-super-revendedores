package br.eti.arnaud.desafioandroidsuperrevendedores.model.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.dao.PullDao
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.dao.RepoDao
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Pull
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Repo

@Database(entities = [Repo::class, Pull::class], version = 14, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun repoDao(): RepoDao
    abstract fun pullDao(): PullDao
}

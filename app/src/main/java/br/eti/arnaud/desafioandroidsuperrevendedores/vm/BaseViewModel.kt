package br.eti.arnaud.desafioandroidsuperrevendedores.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import br.eti.arnaud.desafioandroidsuperrevendedores.model.BaseRepository

abstract class BaseViewModel : ViewModel() {

    var loading: LiveData<Boolean> = MutableLiveData()
    var message: LiveData<Int> = MutableLiveData()

    abstract val repository: BaseRepository

    open fun start() {
        loading = Transformations.map(repository.loading, {
            return@map it
        })
        message = Transformations.map(repository.message, {
            return@map it
        })
    }
}
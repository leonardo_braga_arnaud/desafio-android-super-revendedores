package br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data

import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Repo

data class GithubRepositories(
        val total_count: Long,
        val items: List<Repo>)
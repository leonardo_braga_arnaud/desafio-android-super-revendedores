package br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data.Owner

@Entity
data class Repo(

        @PrimaryKey
        @ColumnInfo(name = "repo_id")
        val id: Long,

        @ColumnInfo(name = "repo_url")
        val url: String,

        @Embedded
        val owner: Owner,

        @ColumnInfo(name = "repo_name")
        val name: String,

        @ColumnInfo(name = "repo_stargazers_count")
        val stargazers_count: Int,

        @ColumnInfo(name = "repo_open_issues_count")
        val open_issues_count: Int,

        @ColumnInfo(name = "repo_language")
        val language: String,

        @ColumnInfo(name = "repo_description")
        val description: String?

)
package br.eti.arnaud.desafioandroidsuperrevendedores.ui.repos

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.eti.arnaud.desafioandroidsuperrevendedores.R
import br.eti.arnaud.desafioandroidsuperrevendedores.ui.BaseFragment
import br.eti.arnaud.desafioandroidsuperrevendedores.vm.ReposViewModel
import com.mugen.Mugen
import kotlinx.android.synthetic.main.fragment_repositories.*

class RepositoriesFragment : BaseFragment() {

    private lateinit var viewModel: ReposViewModel
    private lateinit var adapter: ReposRecyclerViewAdapter
    private lateinit var reposRecyclerViewAdapterListener: ReposRecyclerViewAdapter.Listener

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is ReposRecyclerViewAdapter.Listener){
            reposRecyclerViewAdapterListener = context
            return
        }

        throw IllegalStateException("A activity precisa implementar ReposRecyclerViewAdapter.Listener")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_repositories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(activity!!).get(ReposViewModel::class.java)
        viewModel.start()

        adapter = ReposRecyclerViewAdapter(emptyList(), reposRecyclerViewAdapterListener)

        val columnCount = view.resources.getInteger(R.integer.list_column_count)
        reposRecyclerView.layoutManager =
                if (columnCount == 1) LinearLayoutManager(view.context)
                else GridLayoutManager(view.context, columnCount)
        reposRecyclerView.addItemDecoration(DividerItemDecoration(context,
                DividerItemDecoration.VERTICAL))

        reposRecyclerView.adapter = adapter

        reposSwipeRefresh.setOnRefreshListener { viewModel.refresh() }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.reposObservable.observe(this, Observer {
            it?.let {
                if (it.isEmpty()){
                    emptyTextView.visibility = View.VISIBLE
                    viewModel.refresh()
                } else {
                    emptyTextView.visibility = View.GONE
                    adapter.updateValues(it)
                }
            }
        })

        viewModel.message.observe(this, Observer { error ->
            showSnackbar(error)
            reposSwipeRefresh.isRefreshing = false
        })

        viewModel.loading.observe(this, Observer { b ->
            val isLoading = b == true
            loading(isLoading)
            if (!isLoading) reposSwipeRefresh.isRefreshing = false
        })

        Mugen.with(reposRecyclerView, viewModel.mugenCallbacks).start()

    }



}

package br.eti.arnaud.desafioandroidsuperrevendedores.ui.repos

import android.os.Bundle
import android.view.Menu
import android.view.View
import br.eti.arnaud.desafioandroidsuperrevendedores.R
import br.eti.arnaud.desafioandroidsuperrevendedores.ui.BaseActivity
import br.eti.arnaud.desafioandroidsuperrevendedores.ui.pulls.PullsActivity
import br.eti.arnaud.desafioandroidsuperrevendedores.ui.pulls.PullsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), ReposRecyclerViewAdapter.Listener {

    private var isLandTablet = false
    private var pullsFagment: PullsFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(mainToolbar)

        isLandTablet = resources.getBoolean(R.bool.is_tablet)
                && resources.getBoolean(R.bool.is_land)

        if (isLandTablet){
            pullsFagment = PullsFragment()
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.repo_fragment_container, RepositoriesFragment())
                    .replace(R.id.pull_fragment_container, pullsFagment)
                    .commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun getProgressBar(): View {
        return mainProgressBar
    }

    override fun onRepoItemClick(userName: String, repoName: String) {
        if (isLandTablet){
            pullsFagment?.clearList()
            pullsFagment?.initPullObserver(userName, repoName)
        } else {
            PullsActivity.start(this, userName, repoName)
        }

    }
}

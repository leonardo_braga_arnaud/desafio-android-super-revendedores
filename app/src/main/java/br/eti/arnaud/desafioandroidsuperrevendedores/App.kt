package br.eti.arnaud.desafioandroidsuperrevendedores

import android.app.Application
import br.eti.arnaud.desafioandroidsuperrevendedores.di.AppComponent
import br.eti.arnaud.desafioandroidsuperrevendedores.di.AppModule
import br.eti.arnaud.desafioandroidsuperrevendedores.di.DaggerAppComponent
import net.danlew.android.joda.JodaTimeAndroid


class App : Application() {

    lateinit var appComponent: AppComponent

    companion object {
        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        JodaTimeAndroid.init(this)

        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()

    }

}
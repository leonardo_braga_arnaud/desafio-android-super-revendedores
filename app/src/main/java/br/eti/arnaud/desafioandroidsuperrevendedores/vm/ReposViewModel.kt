package br.eti.arnaud.desafioandroidsuperrevendedores.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.Transformations
import android.content.SharedPreferences
import br.eti.arnaud.desafioandroidsuperrevendedores.App
import br.eti.arnaud.desafioandroidsuperrevendedores.model.ReposRepository
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Repo
import com.mugen.MugenCallbacks
import javax.inject.Inject

class ReposViewModel : BaseViewModel() {

    override val repository = ReposRepository()

    lateinit var sharedPreferences: SharedPreferences
        @Inject set

    private val page = MutableLiveData<Int>()

    private var language = "Java"

    val mugenCallbacks: MugenCallbacks = object : MugenCallbacks {

        override fun onLoadMore() {
            val lastPageLoaded = sharedPreferences.getInt(LAST_PAGE_LOADED, 1)
            page.value = lastPageLoaded + 1

        }

        override fun isLoading(): Boolean {
            return (loading as MutableLiveData<Boolean>).value == true
        }

        override fun hasLoadedAllItems(): Boolean {
            return page.value ?: 1 >= repository.totalPages
        }
    }

    val reposObservable: LiveData<List<Repo>> =
            Transformations.map(repository.reposObservable(language), {
                val currentLastPage = sharedPreferences.getInt(LAST_PAGE_LOADED, 1)
                if (page.value ?: 1 > currentLastPage) {
                    sharedPreferences.edit().putInt(LAST_PAGE_LOADED, page.value ?: 1).apply()
                }
                return@map it
            })

    private val pageObserver = Observer<Int> {
        repository.loadPage(it ?: 1, language)
    }

    init {
        App.instance.appComponent.inject(this)
        page.observeForever(pageObserver)

    }


    override fun onCleared() {
        super.onCleared()
        repository.githubRepositoriesCall?.cancel()
        page.removeObserver(pageObserver)
    }

    fun refresh() {
        page.value = 1
    }

    fun getCurrentPage(): Int {
        return page.value ?: 1
    }

    companion object {
        private const val LAST_PAGE_LOADED = "last_page_loaded"
    }
}
package br.eti.arnaud.desafioandroidsuperrevendedores.vm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import br.eti.arnaud.desafioandroidsuperrevendedores.App
import br.eti.arnaud.desafioandroidsuperrevendedores.model.PullsRepository
import br.eti.arnaud.desafioandroidsuperrevendedores.model.db.entity.Pull

class PullsViewModel : BaseViewModel() {

    override val repository = PullsRepository()

    private var userName: String? = null
    var repoName: String? = null

    init {
        App.instance.appComponent.inject(this)
    }

    override fun onCleared() {
        super.onCleared()
        repository.pullsCall?.cancel()
    }

    fun pullsObservable(userName: String, repoName: String): LiveData<List<Pull>> {

        this.userName = userName
        this.repoName = repoName
        repository.loadData(userName, repoName)

        return Transformations.map(repository.pullsObservable(repoName), {
            return@map it
        })
    }

    fun refresh() {
        if (userName != null && repoName != null){
            repository.loadData(userName!!, repoName!!)
        }
    }

}
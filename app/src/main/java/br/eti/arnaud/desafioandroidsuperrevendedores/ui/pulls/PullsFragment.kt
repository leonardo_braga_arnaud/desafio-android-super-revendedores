package br.eti.arnaud.desafioandroidsuperrevendedores.ui.pulls

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.eti.arnaud.desafioandroidsuperrevendedores.R
import br.eti.arnaud.desafioandroidsuperrevendedores.ui.BaseFragment
import br.eti.arnaud.desafioandroidsuperrevendedores.vm.PullsViewModel
import kotlinx.android.synthetic.main.fragment_repositories.*

class PullsFragment : BaseFragment(), PullsRecyclerViewAdapter.Listener {

    private lateinit var viewModel: PullsViewModel
    private lateinit var adapter: PullsRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pulls, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(activity!!).get(PullsViewModel::class.java)
        viewModel.start()

        adapter = PullsRecyclerViewAdapter(emptyList(), this)

        val columnCount = view.resources.getInteger(R.integer.list_column_count)
        reposRecyclerView.layoutManager =
                if (columnCount == 1) LinearLayoutManager(view.context)
                else GridLayoutManager(view.context, columnCount)
        reposRecyclerView.addItemDecoration(DividerItemDecoration(context,
                DividerItemDecoration.VERTICAL))

        reposRecyclerView.adapter = adapter

        reposSwipeRefresh.setOnRefreshListener { viewModel.refresh() }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.let {
            val userName = it.getString(PullsActivity.USER_NAME)
            val repoName = it.getString(PullsActivity.REPO_NAME)
            initPullObserver(userName, repoName)

        }

        viewModel.message.observe(this, Observer { error ->
            showSnackbar(error)
            reposSwipeRefresh.isRefreshing = false
        })

        viewModel.loading.observe(this, Observer { b ->
            val isLoading = b == true
            loading(isLoading)
            if (!isLoading) reposSwipeRefresh.isRefreshing = false
        })

    }

    fun initPullObserver(userName: String, repoName: String) {
        viewModel.pullsObservable(userName, repoName).observe(this, Observer {
            it?.let {
                if (it.isEmpty()){
                    emptyTextView.visibility = View.VISIBLE
                } else {
                    emptyTextView.visibility = View.GONE
                    adapter.updateValues(it)
                }
            }
        })
    }

    fun clearList(){
        adapter.updateValues(emptyList())
    }

    override fun onPullItemClick(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    companion object {

        fun newInstance(userName: String, repoName: String): PullsFragment{
            val bundle = Bundle()
            bundle.putString(PullsActivity.USER_NAME, userName)
            bundle.putString(PullsActivity.REPO_NAME, repoName)
            val pullsFragment = PullsFragment()
            pullsFragment.arguments = bundle
            return pullsFragment
        }

    }
}

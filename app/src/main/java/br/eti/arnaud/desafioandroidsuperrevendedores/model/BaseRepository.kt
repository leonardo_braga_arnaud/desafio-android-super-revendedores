package br.eti.arnaud.desafioandroidsuperrevendedores.model

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData

/**
 * Dados para contato
 *
 * @author Leonardo Braga Arnaud
 * @email leonardo.arnaud.java@gmail.com
 * @phone (21) 97266-8312
 *
 */
abstract class BaseRepository {

    val loading: LiveData<Boolean> = MutableLiveData()
    val message: LiveData<Int> = MutableLiveData()

    fun setLoading(b: Boolean) {
        (loading as MutableLiveData<Boolean>).value = b
    }

    fun postMessage(strId: Int) {
        (message as MutableLiveData<Int>).postValue(strId)
    }
}
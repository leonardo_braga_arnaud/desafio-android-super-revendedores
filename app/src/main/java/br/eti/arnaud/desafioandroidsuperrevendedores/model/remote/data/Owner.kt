package br.eti.arnaud.desafioandroidsuperrevendedores.model.remote.data

import android.arch.persistence.room.ColumnInfo

data class Owner(

        @ColumnInfo(name = "owner_id")
        val id: Long,

        @ColumnInfo(name = "owner_url")
        val url: String,

        @ColumnInfo(name = "owner_login")
        val login: String,

        @ColumnInfo(name = "owner_avatar_url")
        val avatar_url: String

)
package br.eti.arnaud.desafioandroidsuperrevendedores

import br.eti.arnaud.desafioandroidsuperrevendedores.ui.repos.ReposRecyclerViewAdapter
import org.junit.Assert.assertEquals
import org.junit.Test

class AppUnitTest {

    private val repoMock = TestUtils.createMockRepo()

    @Test
    fun repoAdapterShouldUpdateList() {

        val listener = object : ReposRecyclerViewAdapter.Listener {
            override fun onRepoItemClick(userName: String, repoName: String) {}

        }
        val adapter = ReposRecyclerViewAdapter(listOf(repoMock), listener)
        val updatedList = listOf(repoMock, repoMock)
        try {
            adapter.updateValues(updatedList)
        } catch (ignored: NullPointerException) {}
        assertEquals(2, adapter.itemCount)
    }

}
